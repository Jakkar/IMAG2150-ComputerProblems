from math import *

a = 3344556600
b = 1.2222222
c1 = sqrt((a**2)+(b**2))
diff2 = (b**2)/(sqrt((a**2)+(b**2))+a)

print("a:              ", a)
print("c1:             ", c1)

print("Diff c1-a:      ", c1-a) # trekker fra for like tall, får ikke et riktig svar
print("Diff skrevet om:", diff2)