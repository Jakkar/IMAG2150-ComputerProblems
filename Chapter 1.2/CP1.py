from sympy import symbols, Eq, solveset, S

x = symbols('x', real=True)
equation = (42-(x**3))/5 # omskrevet funksjon der der det er en x alene på en side
value = 5 # some startverdi, bare et sted å begynne (burde være noenlunde nært der man forventer at roten er)
tol = 0.0000000000001

for _ in range(3000): # bare iterer til toleransen er nådd eller bare mange ganger
    result = solveset(Eq(equation, value), x, domain=S.Reals) # setter inn ny x

    if abs(result.args[0] - value) < tol: # avslutt når toleransen er nådd
        print(result.args[0])
        break

    value = float(result.args[0])
    print(value)