from sympy import *

t = symbols('t')
y = symbols('y')

y_diff = 2*exp(t**3) # differential equation
# y_exact = 0.5*t**2+1 # exact equation

y_0 = 2 # intial value
h = 1/4 # step size

w = y_0 # initial w = y(0)

# interval
interval = [0,1]

i = 0
while true: 
    if (i*h >= interval[1]):
        break
    w = w + h*y_diff.subs([(t,i*h),(y,w)])
    i+=1

#err = abs(y_exact.subs(t, (i)*h) - w)
err = 0

print("w_{0}={1} and error at w_{2}: {3}".format(i, round(w, 4), i, round(err, 4)))