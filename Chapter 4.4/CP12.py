import numpy as np


def gramSchmidt(A):
    (n, m) = np.shape(A)
    r = np.zeros((n, m))
    q = np.zeros((n, m))
    for j in range(n):
        y = A[:, j]
        for i in range(j):
            r[i, j] = np.dot(np.transpose(q[:, i]), y)
            y = y - r[i, j] * q[:, i]
        r[j, j] = np.linalg.norm(y, ord=2)
        q[:, j] = y / r[j, j]
    return q, r


A = np.array([[4, 0], [3, 1]])

(Q, R) = gramSchmidt(A)
print(Q)
print(R)
