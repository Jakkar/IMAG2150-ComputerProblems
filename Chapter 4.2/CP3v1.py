import numpy as np

b = np.array([[3039585530], [3707475887], [5281653820], [6079603571]])
A = np.array([[1, 1960], [1, 1970], [1, 1990], [1, 2000]])
b = np.log(b)
ATA = A.T @ A
ATb = A.T @ b
params = np.linalg.solve(ATA, ATb)
print(params)
c2 = params[1]
c1 = np.exp(params[0])
population_func = lambda t: c1 * np.exp(c2 * t)
print(population_func(1980))
