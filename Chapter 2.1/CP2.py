from numpy import *

def gausselim(A):
    (n,m) = shape(A)
    for j in range(n-1):
        for i in range(j+1,n):
            mult = A[i,j] / A[j,j]
            for k in range(j,m):
                A[i,k] = A[i,k]-A[j,k]*mult
    return A

def tilbake_substitusjon(AG):
    (n,m) = shape(AG)
    b = AG[:,-1]
    A = AG[:,:-1]
    x = zeros(n)
    x[n-1] = b[-1]/A[-1,-1]
    for i in range(n-2, -1, -1):
        x[i]=(b[i]-A[i,i+1:n].dot(x[i+1:n]))/A[i,i]
    return x

def create_hilbert_matrix(n):
    H = zeros((n,n))
    for i in range(n):
        for j in range(n):
            H[i,j] = 1/((i+1) + (j+1) - 1)
    return H
    
matrix_size = 2
A = create_hilbert_matrix(matrix_size)
A = concatenate((A,ones((matrix_size,1))),axis=1)

print("Matrise:\n", A)
print("Naiv gauselim matrise:\n", gausselim(A))

print("Svar:", tilbake_substitusjon(gausselim(A)))
