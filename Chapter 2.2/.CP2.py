from numpy import *

def gausselim(A):
    (rows,cols) = shape(A)
    U = array(A)
    L = identity(rows)
    for j in range(rows-1):
        for i in range(j+1,cols):
            if (A[j,j] == 0):
                return
            mult = A[i,j] / A[j,j]
            L[i,j] = mult
            for k in range(j,cols):
                U[i,k] = A[i,k]-A[j,k]*mult

    return L,U

def tilbake_substitusjon(AG):
    (n,m) = shape(AG)
    b = AG[:,-1]
    A = AG[:,:-1]
    x = zeros(n)
    x[n-1] = b[-1]/A[-1,-1]
    for i in range(n-2, -1, -1):
        x[i]=(b[i]-A[i,i+1:n].dot(x[i+1:n]))/A[i,i]
    return x

A = array([[3,7],
           [6,1]])
b = array([[1],[-11]])

L,U = gausselim(A)
print(L)
print(U)

Lb = concatenate((L,b),axis=1)

print(Lb)

print(tilbake_substitusjon(Lb))
