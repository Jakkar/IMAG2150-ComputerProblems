
import numpy as np
from math import *

# shamelessly copied :)
def my_bisection(f, a, b, tol): 

    # sjekk om det finnes en rot på intervallet
    if np.sign(f(a)) == np.sign(f(b)):
        raise Exception(
         "The scalars a and b do not bound a root") 
        
    # midtpunkt på intervall
    m = (a + b)/2
    
    if np.abs(f(m)) < tol:
        # sjekk om man har nådd toleransen
        return m
    elif np.sign(f(a)) == np.sign(f(m)):
        # hvis fortegnet til a er det samme som midtpuinktet
        # kjør rekursivt med ny a = m
        return my_bisection(f, m, b, tol)
    elif np.sign(f(b)) == np.sign(f(m)):
        # hvis fortegnet til b er det samme som midtpuinktet
        # kjør rekursivt med ny b = m
        return my_bisection(f, a, m, tol)


f_1 = lambda x: x**3 - 9

print("Root for x**3 - 9:", my_bisection(f_1, 2,3,0.000001))

f_2 = lambda x: 3*(x**3) + (x**2) - x - 5

print("Root for 3*(x**3) + (x**2) - x - 5:", my_bisection(f_2, 0, 5, 0.000001))

f_3 = lambda x: cos(x)**2 + 6 - x

print("Root for cos(x)**2 + 6 - x:", my_bisection(f_3, 0, 10, 0.000001))

print("Roots are correct to 6 decimal places")
