from math import pi
from sympy import symbols, Subs, diff

x = symbols('x')
func = 2*pi*(x**3)+10*pi*(x**2)-180 # funksjon som er en blanding av volum for kjegle og kule
value = 5 # startverdi

for _ in range(50): # bare mange iterasjoner, burde ha en sjekk for toleranse istedenfor
    value = float(value - (func.subs(x, value) / diff(func, x).subs(x, value))) # bruker newtons moetode for å finne tilnærmet verdi
    print(value)