from numpy import *

def gausselim(A):
    (n,m) = shape(A)
    for j in range(n-1):
        for i in range(j+1,n):
            mult = A[i,j] / A[j,j]
            for k in range(j,m):
                A[i,k] = A[i,k]-A[j,k]*mult
    return A

def tilbake_substitusjon(AG):
    (n,m) = shape(AG)
    b = AG[:,-1]
    A = AG[:,:-1]
    x = zeros(n)
    x[n-1] = b[-1]/A[-1,-1]
    for i in range(n-2, -1, -1):
        x[i]=(b[i]-A[i,i+1:n].dot(x[i+1:n]))/A[i,i]
    return x

A2 = array([[10.0**(-20), 1, 1],
            [1          , 2, 4]])

print("Matrise A2:\n", A2)
print("Naiv gauselim matrise A2:\n", gausselim(A2))

print("Svar A2 (IEEE double precision):", tilbake_substitusjon(gausselim(A2)))

A3 = array([[1          , 2, 4],
            [10.0**(-20), 1, 1]])

print("Matrise A3:\n", A3)
print("Naiv gauselim matrise A3:\n", gausselim(A3))

print("Svar A3 (IEEE double precision, after row exchange):", tilbake_substitusjon(gausselim(A3)))