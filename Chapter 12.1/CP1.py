from numpy import *


def PI(A, x, k):
    for i in range(k):
        u = x / linalg.norm(x)
        x = matmul(A, u)
        l = matmul(matmul(transpose(u), A), u)
    return l, (x / linalg.norm(x))


# a)
A = array([[10, -12, -6], [5, -5, -4], [-1, 0, 3]])

value, vector = PI(A, array([1, 1, 1]), 10)
print("a)\nEigenvalue: ", value, "\nEigenvector: ", vector)

# b)
A = array([[-14, 20, 10], [-19, 27, 12], [23, -32, -13]])

value, vector = PI(A, array([1, 1, 1]), 10)
print("b)\nEigenvalue: ", value, "\nEigenvector: ", vector)
