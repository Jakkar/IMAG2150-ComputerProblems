import numpy as np


x0 = [1960, 1970, 1990, 2000]
y0 = [3039585530, 3707475887, 5281653820, 6079603571]

c = np.polyfit(x0, np.log(y0), deg=1)
print("c: ", c)

y = np.exp(np.polyval(c, x0))

y_1980 = np.exp(np.polyval(c, 1980))

print("y: ", y0)

print(y_1980)
