from numpy import *

# uuuuuh
def CGF(A, B, x_0):
    (n,n) = A.shape
    
    x = []
    r = []
    d = []

    alpha = []
    beta = []

    x.append(x_0)
    r.append(b-A.dot(x[0]))
    d.append(x[0])

    for k in range(n):
        if (linalg.norm(r[k]) == 0):
            break
        
        alpha.append( r[k].dot(r[k]) / d[k].dot( A.dot(r[k]) ))
        x.append( x[k] + alpha[k] + d[k] )
        r.append( r[k] - alpha[k] * A.dot(d[k]))
        beta.append( r[k+1].dot( r[k+1] ) / r[k].dot( r[k] ) )
        d.append( r[k+1] + beta[k] * d[k] )

    return x



A = array([[1,0],
           [0,2]])
b = array([2,4])
x_0 = array([7,7])

x = CGF(A, b, x_0)
print(x[-1])
