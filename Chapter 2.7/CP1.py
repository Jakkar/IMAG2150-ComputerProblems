import numpy as np


def Newtons_multivariate_method(f, df, x0, tol):
    x = x0
    while np.abs(np.linalg.norm(f(x))) > tol:
        fx = f(x)
        dfx = df(x)
        s = np.linalg.solve(dfx, -fx)
        x = x + s
    return x


f = lambda x: np.array(
    [x[0] ** 2 - 4.0 * x[1] ** 2 - 4.0, (x[0] - 1.0) ** 2 + x[1] ** 2 - 4.0]
).reshape((2, 1))

df = lambda x: np.array(
    [2.0 * x[0], -8.0 * x[1], 2.0 * x[0] - 2.0, 2.0 * x[1]]
).reshape((2, 2))

x0 = np.array([1, 1]).reshape((2, 1))

x = Newtons_multivariate_method(f, df, x0, 0.001)

print(f"x:\n {x}")
print(f"feil:\n {np.abs(np.linalg.norm(f(x)))}")
