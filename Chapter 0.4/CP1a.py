from math import *
from tabulate import tabulate

numbers = [10**-1,10**-2,10**-3,10**-4,10**-5,10**-6,10**-7,10**-8,10**-9,10**-10,10**-11,10**-12,10**-13,10**-14]
results = [["x"],["Original expression"],["Modified expression"]]

for num,x in enumerate(numbers):
    results[0].append(numbers[num])
    results[1].append((1 - (1/cos(x)))/(tan(x)**2))
    results[2].append((-1)/(1+(1/cos(x))))

print(tabulate([results[0],results[1],results[2]]))