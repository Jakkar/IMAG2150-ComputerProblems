# 1960, 3039585530
# 1970, 3707475887
# 1990, 5281653820
# 2000, 6079603571

from numpy import *

# years
x0 = [1960, 1970, 1990, 2000]
# actual values
y0 = [3039585530, 3707475887, 5281653820, 6079603571]

# values to estimate
x = 1980

# fit to line and parabole polynomial
c_line = polyfit(x0, y0, 1)
c_par = polyfit(x0, y0, 2)

# estimate value with both polynomials
y_line = polyval(c_line, x)
y_par = polyval(c_par, x)

# estimate original years with both polynomials
y_line_pred = polyval(c_line, x0)
y_par_pred = polyval(c_par, x0)

# print :)
print("Line result:", y_line, "RMSE:", sqrt(square(subtract(y_line_pred, y0)).mean()))
print("Parabole result:", y_par, "RMSE:", sqrt(square(subtract(y_par_pred, y0)).mean()))
