from numpy import *

x = 1.00001
c = ones(51)

def nest(c,x,b=[]):
    d = len(c)-1
    if b==[]:
        b = zeros(d) 
    y = c[d]
    for i in range(d-1,-1,-1):
        y *= (x-b[i])
        y += c[i]
    return y

print("Svar P(X):", nest(c,x))

print("Svar brøk:", ((x**51)-1)/(x-1))

print("Feil:", nest(c,x) - (((x**51)-1)/(x-1)))
