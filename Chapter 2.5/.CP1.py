from numpy import *

def make_matrix(N):
    A = zeros((N,N))
    for i in range(N):
        A[i,i] = 3
        if i == N-1:
            return A
        A[i,i+1] = -1
        A[i+1, i] = -1


def jacobi(A,b,tol,ans,x=None):
    """Solves the equation Ax=b via the Jacobi iterative method."""
    # Create an initial guess if needed                                                                                                                                                            
    if x is None:
        x = zeros(len(A[0]))

    # Create a vector of the diagonal elements of A                                                                                                                                                
    # and subtract them from A                                                                                                                                                                     
    D = diag(A)
    R = A - diagflat(D)

    N = 0                                                                                                                                                                          
    while True:
        x = (b - dot(R,x)) / D
        for i in range(x.size): 
            e = abs(x[i] - ans[i])
            if e > tol:
                 break
            return x, N
        N += 1
        


N = 100
A = make_matrix(N)
b = ones((N))
b[0] = 2
b[N-1] = 2

answer = ones(100)
ans, iterations = jacobi(A, b, 0.0000001, answer)
print(ans)
print(iterations)