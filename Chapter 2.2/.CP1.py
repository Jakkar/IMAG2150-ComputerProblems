from numpy import *

def gausselim(A):
    (rows,cols) = shape(A)
    U = array(A)
    L = identity(rows)
    for j in range(rows-1):
        for i in range(j+1,cols):
            if (A[j,j] == 0):
                return
            mult = A[i,j] / A[j,j]
            L[i,j] = mult
            for k in range(j,cols):
                U[i,k] = A[i,k]-A[j,k]*mult

    return L,U

A = array([[4,2,0],
           [4,4,2],
           [2,2,3]])


L,U = gausselim(A)
print(L)
print(U)